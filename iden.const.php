<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-23 22:08
 *
 * 项目：upload  -  $  - iden.const.php
 *
 * 作者：liwei 
 */

!defined('IN_DISCUZ') && exit('Access Denied');

defined('APPVIDEN') or define('APPVIDEN', 'levs');

defined('LEVROOT') or define('LEVROOT', dirname(__DIR__) . '/'.APPVIDEN.'/lev');

defined('APPVROOT') or define('APPVROOT', dirname(__DIR__) . '/'. APPVIDEN);

//定义模块标识 - 非前置应用无需定义
defined('MODULEIDEN') or define('MODULEIDEN', basename(__DIR__));
