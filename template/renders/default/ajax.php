<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-09-03 15:15
 *
 * 项目：rm  -  $  - ajax.php
 *
 * 作者：liwei 
 */

?>


<?php foreach ($lists as $v):?>

    <div class="card card-bduid-<?=$v['id']?>">
        <a class="goods-box bduLoginBtn" data-uid="<?=$v['bduid']?>">
            <iconi><img class="lazy" data-src="<?=\lev\helpers\UserHelper::avatar($v['bduid'])?>"></iconi>
            <?=$v['bduid'] == Lev::$app['uid'] ? '<span>当前</span>' : ($v['bduid'] == $v['uid'] ? '<span>主</span>' : '')?>
            <num class="hiddenx delBduBtn inblk scale6" data-id="<?=$v['id']?>"><svg class="icon scale7"><use xlink:href="#fa-trash"></use></svg></num>
        </a>
        <a class="money-box" style="overflow: hidden;text-overflow: ellipsis;">
        <span class="scale8 inblk">
            <?=Lev::arrv([$v['bduid'], 'username'], $users)?>
        </span>
        </a>
    </div>

<?php endforeach;?>
