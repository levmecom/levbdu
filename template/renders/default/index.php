<?php

?>

<div class="page">
    <style>
        .levbdu-mbox .virtual-list,
        .levbdu-mbox .listLoadBox {display: flex;flex-wrap: wrap;justify-content: space-around;}
        .levbdu-mbox.cards-box .card {width: 95px;height: 95px;}
        .levbdu-mbox.cards-box .goods-box img {width: 93px;height: 93px;}
    </style>

    <?php Lev::navbar(); Lev::toolbar();?>

    <div class="page-content appbg infinite-scroll levbduListInfBox" style="position: relative !important;">
        <div class="page-content-inner" style="max-width:660px;background:rgba(0,0,0,0.2);max-height:min-content;overflow: hidden;min-height:100%">
            <!--幻灯片-->
            <div class="slides-mbox">
                <?php echo \lev\widgets\slides\slidesWidget::run()?>
            </div>

            <div class="card-header">
                <span class="color-white scale9 transl">轻触头像以切换账号</span>
                <div class="flex-box">
                <a class="button button-fill color-red addBduBtn scale8">
                    <svg class="icon"><use xlink:href="#fa-add"></use></svg>
                    添加账号
                </a>
                    <a class="button button-fill color-gray scale8 delBduBtnShow">
                        <svg class="icon"><use xlink:href="#fa-trash"></use></svg>
                    </a>
                </div>
            </div>

            <div class="levbdu-mbox list-block cards-box">
                <?php echo \lev\widgets\infiniteLoad\infiniteLoadWidget::run('.levbduListInfBox', Lev::toReRoute(['default/ajax', 'id'=>'levbdu']), 1)?>
            </div>


        </div>

    </div>

<div class="LoadPageAjaxJS">
<script>
(function () {
    'use strict';

    jQuery(function () {
        Levme.onClick('.delBduBtnShow', function () {
            if (jQuery(this).hasClass('isShowd')) {
                jQuery(this).removeClass('isShowd');
                jQuery('.delBduBtn').addClass('hiddenx').hide();
            }else {
                jQuery(this).addClass('isShowd');
                jQuery('.delBduBtn').removeClass('hiddenx').show();
            }
        });
        Levme.onClick('.delBduBtn', function () {
            var obj = this;
            myApp.confirm('您确定要删除吗？', function () {
                var opid = jQuery(obj).data('id');
                Levme.ajaxv.getv(levToRoute("<?=Lev::toReRoute(['default/delbdu', 'id'=>'levbdu'])?>", {opid:opid}), function (data, status) {
                    if (status > 0) {
                        jQuery('.card-bduid-'+ opid).fadeOut();
                    }
                });
            });
            return false;
        });

        Levme.onClick('.bduLoginBtn', function () {
            if (jQuery('.delBduBtnShow').hasClass('isShowd')) return;

            var uid = jQuery(this).data('uid');
            Levme.ajaxv.getv(levToRoute("<?=Lev::toReRoute(['default/login', 'id'=>'levbdu'])?>", {uid:uid}), function (data, status) {
                if (status > 0) {
                    window.setTimeout(function (){window.location.reload()}, 800);
                }
            });
        });

        Levme.onClick('.addBduBtn', function () {
            Levme.ajaxv.getv('<?=Lev::toReRoute(['default/add-bdu', 'id'=>'levbdu'])?>', function (data, status) {
                if (status > 0) {
                    data.location && window.setTimeout(function (){window.location = data.location}, 800);
                    //data._csrf && (_csrf = data._csrf);
                    //openLoginScreen(0,1,1);
                }
            });
        });

        UID <1 && openLoginScreen(0,1,1);

        <?=$bdid || $bdu ? 'SaveBdu();' : ''?>
        function SaveBdu() {
            window.setTimeout(function () {
            Levme.ajaxv.getv('<?=Lev::toReRoute(['default/add-bdu', 'id'=>'levbdu', 'bdu'=>$bdu, 'bdid'=>$bdid])?>', function (data, status) {
                if (status > 0) {
                    window.setTimeout(function (){window.location = "<?=Lev::toReRoute(['/', 'id'=>'levbdu'])?>";}, 800);
                }
            });
            }, 800);
        }
    });
})();
</script>
</div>
</div>



