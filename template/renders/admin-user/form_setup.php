<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-15 15:39
 *
 * 项目：upload  -  $  - form.php
 *
 * 作者：liwei 
 */

!defined('INLEV') && exit('Access Denied LEV');

?>


<div class="page page-formb page-admin">
    <?php Lev::navbarAdmin($addurl, $srhtitle, $srhkey, 1, '', 1); ?>

    <div class="page-content">
        <div class="page-content-inner" style="max-width:1000px">
            <?php if (count($setupDesc) >1):?>
        <div class="card">
            <div class="card-content-inner buttons-row setups setupsBox" opid="<?=$opid?>">
                <?php foreach ($setupDesc as $k => $name):?>
                    <a class="button <?=$k==$setup ? 'active animated shake' : 'color-gray'?>" href="<?=Lev::toCurrent(['setup'=>$k])?>"><?=$name?></a>
                <?php endforeach;?>
            </div>
        </div>
            <?php endif;?>

        <div class="form-mainb" style="min-width: 660px">
            <form id="saveForm" class="card" action="" method="post">

                <?php echo \lev\widgets\inputs\inputsWidget::form($inputs, $inputsValues, $formPre);?>

                <div class="card-footer">
                    <button type="submit" id="dosubmit" class="button-fill button wd100 dosaveFormBtn">
                        <svg class="icon" aria-hidden="true"><use xlink:href="#fa-save"></use></svg>
                        保 存
                    </button>
                    <tips>【提示】仅添加一条数据，不会检查用户UID是否存在</tips>
                </div>
            </form>
        </div>
        </div>
    </div>

</div>

