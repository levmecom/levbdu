<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-15 13:10
 *
 * 项目：upload  -  $  - index.php
 *
 * 作者：liwei 
 */

!defined('INLEV') && exit('Access Denied LEV');

?>

<div class="page page-admin">
    <?php Lev::toolbarAdmin(); Lev::navbarAdmin($addurl, $srhtitle, $srhkey, 1, '', 0, 1); ?>

    <div class="page-content">
        <div class="card data-listb">
            <div class="data-xtable">
                <table><thead><tr>
                        <th class="checkbox-cell tab-center wd30">
                            <input type="checkbox" name="opids" onclick="checkedToggle(this,'input[name=\'ids[]\']')">
                        </th>
                        <th class="wd120">用户UID</th>
                        <th class="wd60">绑定用户UID</th>
                        <th class="tab-center wd60">状态</th>
                        <th class="numeric-cell wd60">加入时间</th>
                        <th class="">操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($pages['lists'])):foreach ($pages['lists'] as $v):?>
                        <tr>
                            <td class="checkbox-cell tab-center"><input name="ids[]" type="checkbox" value="<?php echo $v['id']?>" title="<?php echo $v['id']?>" autocomplete="off"></td>
                            <td>
                                <a href="<?php echo Lev::toCurrent(['uid'=>$v['uid']])?>">
                                    <?php echo $v['uid'],'@<br>',Lev::arrv([$v['uid'], 'username'], $users, '#已丢失#')?>
                                </a>
                            </td>
                            <td>
                                <a href="<?php echo Lev::toCurrent(['bduid'=>$v['bduid']])?>">
                                    <?php echo $v['bduid'],'@<br>',Lev::arrv([$v['bduid'], 'username'], $users, '#已丢失#')?>
                                </a>
                            </td>
                            <td class="tab-center">
                                <label class="label-switch scale8 color-green setStatus" opid="<?php echo $v['id']?>">
                                    <input type="checkbox" <?=$v['status']?'':'checked'?>>
                                    <div class="checkbox"></div>
                                </label>
                            </td>
                            <td class="numeric-cell">
                                <p class="date transr" title="加入时间"><?php echo Lev::asRealTime($v['addtime'], '从未')?></p>
                            </td>
                            <td class="">
                                <?php if ($addurl):?>
                                <a href="<?php echo Lev::toRoute([$addurl, 'opid'=>$v['id']])?>">
                                    <absxb>编辑</absxb>
                                </a>
                                <?php endif;?>
                            </td>
                        </tr>
                    <?php endforeach; else:?>
                        <tr><td colspan="100" class="tab-center">
                                <tips><?php echo $srhkey?'没有搜索到【'.$srhkey.'】相关数据':'没有数据'?></tips>
                            </td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
                <div class="card-footer">
                    <tips></tips>
                    <?php echo $pages['pages']?>
                </div>
            </div>
        </div>
    </div>

</div>
