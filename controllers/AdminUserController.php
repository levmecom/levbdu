<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：20210903
 *
 * 项目：rm  -  $  - AdminUserController.php
 *
 * 作者：liwei 
 */


namespace modules\levbdu\controllers;

use Lev;
use lev\base\Adminv;
use lev\base\Assetsv;
use lev\base\Controllerv;
use lev\base\Modelv;
use lev\base\Viewv;
use lev\helpers\UserHelper;
use modules\levbdu\table\bduHelper;

!defined('INLEV') && exit('Access Denied LEV');

Adminv::checkAccess();
Assetsv::registerSuperman();

class AdminUserController extends Controllerv
{

    public static $tmpPath = 'admin-user';

    /**
     * @return string
     */
    public static function modelv() {
        return '\modules\levbdu\table\bduHelper';
    }

    /**
     * Renders the index view for the module
     * @see Modelv::adminop()
     * @see Modelv::pageButtons()
     */
    public function actionIndex()
    {
        $modelv = static::modelv();

        if ($adminop = Lev::POSTv('adminop')) {
            if (($tips = $modelv::adminop($adminop)) !== null) {
                echo json_encode($tips);
                return;
            }
        }
        $srhkey = Lev::stripTags(Lev::GETv('srhkey'));
        $srhkey = addcslashes($srhkey, '%_');
        if ($srhkey) {
            $pages = bduHelper::myBdus($srhkey, 20, false);
        }else {
            $where = [];
            $uid = floatval(Lev::GETv('uid'));
            $uid > 0 && $where['uid'] = $uid;
            $bduid = floatval(Lev::GETv('bduid'));
            $bduid > 0 && $where['bduid'] = $bduid;
        }

        $limit = 20;
        $order = ['id DESC'];
        $pages = isset($pages) ? $pages : $modelv::pageButtons($where?:1, $limit, $order);
        $users = UserHelper::getUsers($pages['lists'], ['uid', 'bduid']);
        $users[0] = ['uid'=>0, 'username'=>'#匿名#'];

        Lev::$app['title'] = '切换用户 管理';
        $srhtitle = '查询UID可切换账号';

        Viewv::render(static::$tmpPath.'/index', [
            'pages'     => $pages,
            'users'     => $users,
            'srhkey'    => $srhkey,
            'srhtitle'  => $srhtitle,
            'addurl'    => Lev::toReRoute([static::$tmpPath.'/form']),
        ]);

    }

    /**
     * Renders the index view for the module
     * @see Modelv::findOne()
     * @see Modelv::inputsSetup()
     * @see Modelv::setupDesc()
     */
    public function actionForm() {
        $formPre = 'datax';

        if (Lev::POSTv('dosubmit')) {
            parent::csrfValidation();
            echo json_encode(static::save($formPre));
            return;
        }

        $modelv = static::modelv();

        $opid = intval(Lev::GETv('opid'));
        $setup = Lev::stripTags(Lev::GETv('setup'));

        $opInfo = $opid >0 ? $modelv::findOne(['id'=>$opid]) : [];
        //$inputs = $modelv::inputs();
        $inputsSetup = $modelv::inputsSetup();
        $setupDesc = $modelv::setupDesc(!$opInfo);
        $inputs = isset($inputsSetup[$setup]) ? $inputsSetup[$setup] : reset($inputsSetup);

        if ($opInfo) {
            $opInfo = Modelv::setFormSettings($opInfo['settings'], $opInfo);
            Lev::$app['title'] = '编辑ID：'.$opInfo['id'];
        }else {
            Lev::$app['title'] = '创建';
        }

        Assetsv::animateCss();
        Viewv::render(static::$tmpPath.'/form_setup', [
            'opid' => $opid,
            'inputs' => $inputs,
            'setupDesc' => $setupDesc,
            'inputsValues' => $opInfo,
            'formPre' => $formPre,
            'setup' => $setup,
        ]);

    }

    /**
     * @param string $formPre
     * @return array
     * @see Modelv::findOne()
     * @see Modelv::safeColumnsInpus()
     * @see Modelv::saveInputs()
     * @see Modelv::getNextSetup()
     */
    public static function save($formPre = 'datax')
    {
        $upData = Lev::stripTags(Lev::POSTv($formPre));

        $modelv = static::modelv();

        $opid = floatval(Lev::GPv('opid'));

        $opInfo = $opid >0 ? $modelv::findOne(['id'=>$opid]) : [];

        $upData['settings'] = $opInfo ? Lev::getSettings($opInfo['settings']) : [];
        $upData = $modelv::safeColumnsInpus($upData);
        //$upData = $modelv::safeColumns($upData);

        $upData['uid'] = floatval($upData['uid']);
        if (!$upData['uid']) {
            return Modelv::errorMsg2($formPre.'[uid]', -2204, '用户UID不能为空');
        }
        $upData['bduid'] = floatval($upData['bduid']);
        if (!$upData['bduid']) {
            return Modelv::errorMsg2($formPre.'[bduid]', -2205, '绑定用户UID不能为空');
        }

        $ckuid = bduHelper::findOne("uid={$upData['uid']} AND bduid={$upData['bduid']} AND id<>$opid");
        if ($ckuid) {
            return Lev::responseMsg(-2206, '已经存在相同绑定');
        }

        $upData['uptime'] = Lev::$app['timestamp'];
        if ($opInfo) {
            $rs = $modelv::saveInputs($upData, ['id'=>$opid]);
        }else {
            $upData['addtime'] = Lev::$app['timestamp'];
            $rs = $modelv::saveInputs($upData, [], true);
            $opid = $upData['id'] = $rs;
        }
        if ($rs) {
            $tourl = '';//Lev::toReRoute([static::$tmpPath.'/form', 'opid'=>$opid, 'setup'=>$modelv::getNextSetup()]);
            return Lev::responseMsg(1, '保存成功', ['opid'=>$opid, 'tourl'=>$tourl]);
        }
        return Lev::responseMsg(-2203, '保存失败');
    }


}