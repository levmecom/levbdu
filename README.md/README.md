
仿微信换个账号登陆，可以保存多个账号，一键切换


【功能介绍】
1.后台可设置用户可以登陆切换账号上限
2.后台可设置禁止用户组使用此功能
3.后台管理员可添加任意账号，无上限
4.用户可随意添加删除切换账号，支持三方登陆

演示地址：https://dz.levme.com/plugin.php?id=levbdu


