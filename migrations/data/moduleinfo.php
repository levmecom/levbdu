<?php !defined('INLEV') && exit('Access Denied LEV');
 return array(
'name'=>'仿微信换个账号登陆',
'identifier'=>'levbdu',
'classdir'=>'0',
'descs'=>'保存账号登陆、多账号切换账号',
'copyright'=>'Lev',
'version'=>'20210903.02',
'versiontime'=>'1638420703',
'settings'=>serialize(array (
  '_adminNavs' => 
  array (
    1 => 
    array (
      'id' => 1,
      'order' => '1',
      'name' => '账号管理',
      'target' => '1',
      'status' => '0',
      'tableName' => '0',
      'link' => 'admin-user',
      'forceGen' => '',
    ),
  ),
  '_adminClassify' => 
  array (
    1 => 
    array (
      'id' => '1',
      'order' => '1',
      'status' => '0',
      'name' => '权限配置',
    ),
  ),
  'dropTables' => 
  array (
    0 => '{{%levbdu}}',
  ),
)),
'displayorder'=>'0',
'status'=>'0',
'uptime'=>'1638413510',
'addtime'=>'1638413359',
);;