<?php
/* 
 * Copyright (c) 2018-2021   All rights reserved.
 * 
 * 创建时间：2021-05-27 16:34
 *
 * 项目：upload  -  $  - setHelper.php
 *
 * 作者：liwei 
 */

namespace modules\levbdu\helpers;

use Lev;

!defined('INLEV') && exit('Access Denied LEV');

class BaselevbduSet {

    public static function maxbdu() {
        return floatval(Lev::stget('maxbdu', 'levbdu'));
    }

    public static function groupids() {
        return (Lev::stget('groupids', 'levbdu'));
    }

    public static function bdupwd() {
        return (Lev::stget('bdupwd', 'levbdu'));
    }

}

class levbduSetHelper extends BaselevbduSet {}